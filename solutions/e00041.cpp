/**
 * @file e00041.cpp
 * @copyright (C) 2018 by Oleh Kurachenko.
 * See LICENCE file at the root of repository
 * @author Oleh Kurachenko <oleh.kurachenko@gmail.com>
 * @date Created 2018-10-15
 *
 * @see Author's <a href="gitlab.com/oleh.kurachenko">GitLab</a>
 * @see Author's
 * <a href="linkedin.com/in/oleh-kurachenko-6b025b111">LinkedIn</a>
 */

#include <iostream>
#include <cstdint>

#include <bitset>

#include <vector>

using namespace std;

size_t n;
vector< uint64_t > table;

struct SearchState {
    uint64_t used;
    uint64_t length;
};

SearchState findMaxTeam(
        SearchState previous,
        size_t position) {
    if (position == n)
        return previous;
    if ((previous.used & table[position]) != previous.used) {
        return findMaxTeam(previous, position + 1);
    } else {
        SearchState thisUsed = findMaxTeam(
                {previous.used | (1ull << position), previous.length + 1},
                position + 1);
        SearchState thisNotUsed = findMaxTeam(previous, position + 1);
        if (thisUsed.length >= thisNotUsed.length)
            return thisUsed;
        else
            return thisNotUsed;
    }
}

int main() {
    // configuring iostream
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    cin >> n;

    table.resize(n);
    for (size_t i{0}; i < n; ++i) {
        table[i] = 1ull << i;
    }

    size_t m;
    cin >> m;
    for (size_t i{0}; i < m; ++i) {
        size_t temp1, temp2;
        cin >> temp1 >> temp2;
        --temp1;
        --temp2;
        table[temp1] |= 1ull << temp2;
        table[temp2] |= 1ull << temp1;
    }

    //for (size_t i{0}; i < n; ++i)
    //    cout << "i = " << bitset<64>(table[i]) << endl;

    SearchState result = findMaxTeam({0, 0}, 0);
    bitset<64> resultBitset(result.used);
    //cout << "res = " << bitset<64>(result.used) << endl;

    cout << result.length << endl;
    for (size_t i{0}; i < n; ++i)
        if (resultBitset[i])
            cout << (i + 1) << ' ';
    cout << endl;

    return 0;
}
