/**
 * @file e01229.cpp
 * @copyright (C) 2018 by Oleh Kurachenko.
 * See LICENCE file at the root of repository
 * @author Oleh Kurachenko <oleh.kurachenko@gmail.com>
 * @date Created 2018-11-14
 * @date Submitted 2018-11-15
 *
 * @see Author's <a href="gitlab.com/oleh.kurachenko">GitLab</a>
 * @see Author's
 * <a href="linkedin.com/in/oleh-kurachenko-6b025b111">LinkedIn</a>
 */

#include <cstdint>
#include <cmath>

#include <iostream>
#include <vector>

using namespace std;

// global objects
const size_t number_limit{ 4000000 };

vector<uint32_t> divisor(number_limit + 1, 0);
vector<uint32_t> prime;

vector<uint32_t> euler_numbers(number_limit + 1, UINT32_MAX);

inline uint32_t euler_number(const uint32_t i) {
    if (euler_numbers[i] != UINT32_MAX)
        return euler_numbers[i];

    uint32_t temp{i}, result{i}, temp_divisor{UINT32_MAX};
    while (temp != 1) {
        result -= result / divisor[temp];
        temp_divisor = divisor[temp];
        while (divisor[temp] == temp_divisor)
            temp /= divisor[temp];
    }
    return euler_numbers[i] = result;
}

int main() {
    ios::sync_with_stdio(false);

    // filling divisor and prime vectors
    for (size_t i{2}; i <= number_limit; ++i) {
        if (divisor[i] == 0) {
            divisor[i] = static_cast<uint32_t>(i);
            prime.push_back(static_cast<uint32_t>(i));
        }
        for (size_t j{0}; j < prime.size() && prime[j] <= divisor[i]
                && prime[j] * i <= number_limit; ++j)
            divisor[prime[j] * i] = prime[j];
    }

    vector<uint64_t> g(number_limit + 1, 0);

    // filling g
    for (uint32_t i{1}; i <= number_limit; ++i)
        for (uint32_t j{2}; i * j <= number_limit; ++j)
            g[i * j] += i * euler_number(j);
    for (size_t i{2}; i <= number_limit; ++i)
        g[i] += g[i - 1];

    do {
        uint32_t n;
        cin >> n;

        if (!n)
            break;

        cout << g[n] << endl;
    } while (true);
}