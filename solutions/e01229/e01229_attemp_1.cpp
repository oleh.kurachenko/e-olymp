/**
 * @file e01229.cpp
 * @copyright (C) 2018 by Oleh Kurachenko.
 * See LICENCE file at the root of repository
 * @author Oleh Kurachenko <oleh.kurachenko@gmail.com>
 * @date Created 2018-11-14
 *
 * @see Author's <a href="gitlab.com/oleh.kurachenko">GitLab</a>
 * @see Author's
 * <a href="linkedin.com/in/oleh-kurachenko-6b025b111">LinkedIn</a>
 */

#include <cstdint>
#include <cmath>

#include <iostream>
#include <vector>

// test include
#include <unistd.h>

using namespace std;

// global objects
const size_t number_limit{ 4000000 };

vector<uint32_t> divisor(number_limit + 1, 0);
vector<uint32_t> prime;

vector<uint32_t> euler_numbers(number_limit + 1, UINT32_MAX);

uint64_t gcd(const uint64_t a, const uint64_t b) {
    return (b) ? gcd(b, a % b) : a;
}

inline uint32_t euler_number(const uint32_t i) {
    if (euler_numbers[i] != UINT32_MAX)
        return euler_numbers[i];

    uint32_t temp{i}, result{i}, temp_divisor{UINT32_MAX};
    while (temp != 1) {
        result -= result / divisor[temp];
        temp_divisor = divisor[temp];
        while (divisor[temp] == temp_divisor)
            temp /= divisor[temp];
    }
    return euler_numbers[i] = result;
}

int main() {
    ios::sync_with_stdio(false);

    // filling divisor and prime vectors
    for (size_t i{2}; i <= number_limit; ++i) {
        if (divisor[i] == 0) {
            divisor[i] = static_cast<uint32_t>(i);
            prime.push_back(static_cast<uint32_t>(i));
        }
        for (size_t j{0}; j < prime.size() && prime[j] <= divisor[i]
                && prime[j] * i <= number_limit; ++j)
            divisor[prime[j] * i] = prime[j];
    }

//    for (uint32_t i = 1; i <= number_limit; ++i)
//        euler_number(i);

    vector<uint64_t> g(number_limit + 1, 0);

    for (uint32_t i{2}; i <= number_limit; ++i) {
        //if (i % 10000 == 0)
        //    cout << "at " << i << endl;

        if (divisor[i] == i) {
            g[i] += i - 1 + g[i - 1];

            if (i < 20)
                cout << "at " << i << " res = " << (g[i] - g[i - 1]) << endl;

            continue;
        }

        double sqrt_i_d(sqrt(i));
        uint32_t sqrt_i(static_cast<uint32_t>(sqrt_i_d));
        if (sqrt_i * sqrt_i == i) {
            g[i] += euler_number(sqrt_i) * sqrt_i;
        } else {
            ++sqrt_i;
        }

        for (uint32_t j = 2; j < sqrt_i; j += 1)
            if (i % j == 0) {
                uint32_t back_j{i / j};
                g[i] += euler_number(j) * back_j;
                g[i] += euler_number(back_j) * j;
            }

        g[i] += euler_number(i);

        if (i < 20)
            cout << "at " << i << " res = " << g[i] << endl;

        g[i] += g[i - 1];
    }

    // cout << "ready" << endl;

    do {
        uint32_t n;
        cin >> n;

        if (!n)
            break;

        cout << g[n] << endl;
    } while (true);
}