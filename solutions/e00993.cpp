/**
 * @file e00993.cpp
 * @copyright (C) 2018 by Oleh Kurachenko.
 * See LICENCE file at the root of repository
 * @author Oleh Kurachenko <oleh.kurachenko@gmail.com>
 * @date Created 2018-10-15
 *
 * @see Author's <a href="gitlab.com/oleh.kurachenko">GitLab</a>
 * @see Author's
 * <a href="linkedin.com/in/oleh-kurachenko-6b025b111">LinkedIn</a>
 */

#include <iostream>
#include <cstdint>

#include <vector>

using namespace std;

int main() {
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    size_t n, m;
    cin >> n >> m;

    vector<size_t> result(n, 0);
    for (size_t i{0}; i < m; ++i) {
        size_t v1, v2;
        cin >> v1 >> v2;

        ++result[v1 - 1];
        ++result[v2 - 1];
    }

    for (size_t i{0}; i < n; ++i) {
        cout << result[i] << ' ';
    }
    cout << '\n';

    return 0;
}
