/**
 * @file e01244.cpp
 * @copyright (C) 2018 by Oleh Kurachenko.
 * See LICENCE file at the root of repository
 * @author Oleh Kurachenko <oleh.kurachenko@gmail.com>
 * @date Created 2018-11-15
 * @date Submitted 2018-11-15
 *
 * @see Author's <a href="gitlab.com/oleh.kurachenko">GitLab</a>
 * @see Author's
 * <a href="linkedin.com/in/oleh-kurachenko-6b025b111">LinkedIn</a>
 */

#include <cstdint>

#include <iostream>

using namespace std;

uint32_t gcd(const uint32_t a, const uint32_t b) {
    return (b == 0) ? a : gcd(b, a % b);
}

int main() {
    ios::sync_with_stdio(false);

    uint32_t a, b;

    cin >> a >> b;

    if (b % a != 0) {
        cout << 0 << endl;
        return 0;
    }

    uint32_t product = b / a;

    uint64_t result{0};

    for (uint32_t i = 1; i <= product; ++i)
        if (product % i == 0 && gcd(i, product / i) == 1)
            ++result;

    cout << result << endl;
}
 
