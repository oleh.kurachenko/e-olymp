/**
 * @file e00136.cpp
 * @copyright (C) 2018 by Oleh Kurachenko.
 * See LICENCE file at the root of repository
 * @author Oleh Kurachenko <oleh.kurachenko@gmail.com>
 * @date Created 2018-11-15
 * @date Submitted 2018-11-15
 *
 * @see Author's <a href="gitlab.com/oleh.kurachenko">GitLab</a>
 * @see Author's
 * <a href="linkedin.com/in/oleh-kurachenko-6b025b111">LinkedIn</a>
 */

#include <cstdint>
#include <cmath>

#include <iostream>

using namespace std;

template <typename INT_T>
INT_T gcd(const INT_T a, const INT_T b) {
    return (b == 0) ? a : gcd(b, a % b);
}

int main() {
    int64_t x1, y1, x2, y2;
    cin >> x1 >> y1 >> x2 >> y2;

    uint64_t dx = static_cast<uint64_t>(abs(x1 - x2)),
             dy = static_cast<uint64_t>(abs(y1 - y2));

    cout << gcd(dx, dy) + 1 << endl;
}