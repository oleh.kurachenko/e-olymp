/**
 * @file e08593.cpp
 * @copyright (C) 2018 by Oleh Kurachenko.
 * See LICENCE file at the root of repository
 * @author Oleh Kurachenko <oleh.kurachenko@gmail.com>
 * @date Created 2018-11-06
 *
 * @see Author's <a href="gitlab.com/oleh.kurachenko">GitLab</a>
 * @see Author's
 * <a href="linkedin.com/in/oleh-kurachenko-6b025b111">LinkedIn</a>
 */

#include <cstdint>

#include <iostream>
#include <string>
#include <algorithm>
#include <vector>

using namespace std;

bool comp(const string &a, const string &b) {
    string a1(a), b1(b);

    sort(a1.begin(), a1.end());
    sort(b1.begin(), b1.end());

    if (a1 != b1)
        return a1 > b1;
    return a < b;
}

int main() {
    ios::sync_with_stdio(false);

    string number;
    cin >> number;

    vector<int> digits;
    digits.reserve(number.length());

    for (const auto &digit : number)
        digits.push_back(digit - '0');

    sort(digits.begin(), digits.end());

    if (digits.size() == 1) {
        if (digits[0] % 8 == 0)
            cout << digits[0] << endl;
        else
            cout << "-1" << endl;
        return 0;
    }

    if (digits.size() == 2) {
        if (digits[0] != 0 && (digits[0] * 10 + digits[1]) % 8 == 0)
            cout << digits[0] << digits[1] << endl;
        else if ((digits[1] * 10 + digits[0]) % 8 == 0)
            cout << digits[1] << digits[0] << endl;
        else
            cout << "-1" << endl;
        return 0;
    }

    vector<int16_t> unique(10, 0);

    for (const auto &digit : digits)
        ++unique[digit];

    vector<string> v;
    for (int k = 0; k < 1000; k += 8) {
        v.push_back(to_string(k));
        while (v.back().length() != 3)
            v.back() = "0" + v.back();
    }

    sort(v.begin(), v.end(), comp);

    for (auto vel : v) {
        int d1, d2, d3;

        d3 = vel[2] - '0';
        d2 = vel[1] - '0';
        d1 = vel[0] - '0';

        if (digits.size() == 3 && d1 == 0)
            continue;

        --unique[d1];
        --unique[d2];
        --unique[d3];

        bool isOk = true;

        for (size_t i = 0; i < 10; ++i)
            if (unique[i] < 0) {
                isOk = false;
                break;
            }

        if (unique[0] > 0 && unique[0] == digits.size() - 3)
            isOk = false;

        if (isOk) {
            for (size_t i = 1; i <= 9; ++i)
                if (unique[i] > 0) {
                    cout << i;
                    --unique[i];
                    break;
                }
            for (size_t i = 0; i <= 9; ++i)
                for (size_t j = 0; j < unique[i]; ++j)
                    cout << i;
            cout << d1 << d2 << d3 << endl;
            return 0;
        }

        ++unique[d1];
        ++unique[d2];
        ++unique[d3];
    }

    cout << "-1" << endl;

    return 0;
}
