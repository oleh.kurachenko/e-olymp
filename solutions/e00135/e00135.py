##
#  @file e00135.py
#  @copyright (C) 2018 by Oleh Kurachenko.
#  See LICENCE file at the root of repository
#  @author Oleh Kurachenko <oleh.kurachenko@gmail.com>
#  @date Created 2018-11-15
#  @date Submitted 2018-11-15
#
#  @see Author's <a href="gitlab.com/oleh.kurachenko">GitLab</a>
#  @see Author's
#  <a href="linkedin.com/in/oleh-kurachenko-6b025b111">LinkedIn</a>
#


def gcd(a: int, b: int) -> int:
    if b == 0:
        return a
    return gcd(b, a % b)


if __name__ == '__main__':
    n = int(input())

    string = input().split()
    result = int(string[0])

    for i in range(1, n):
        result *= int(int(string[i]) / gcd(result, int(string[i])))

    print(result)
