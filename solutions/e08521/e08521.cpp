/**
 * @file e08521.cpp
 * @copyright (C) 2018 by Oleh Kurachenko.
 * See LICENCE file at the root of repository
 * @author Oleh Kurachenko <oleh.kurachenko@gmail.com>
 * @date Created 2018-11-06
 *
 * @see Author's <a href="gitlab.com/oleh.kurachenko">GitLab</a>
 * @see Author's
 * <a href="linkedin.com/in/oleh-kurachenko-6b025b111">LinkedIn</a>
 */

#include <cstdint>

#include <iostream>

using namespace std;

int main() {
    ios::sync_with_stdio(false);

    int64_t x;
    cin >> x;

    if (x >= 10)
        x = x * x * x + 5 * x;
    else
        x = x * x - 2 * x + 4;

    cout << x << endl;

    return 0;
}
