/**
 * @file e00992.cpp
 * @copyright (C) 2018 by Oleh Kurachenko.
 * See LICENCE file at the root of repository
 * @author Oleh Kurachenko <oleh.kurachenko@gmail.com>
 * @date Created 2018-10-15
 *
 * @see Author's <a href="gitlab.com/oleh.kurachenko">GitLab</a>
 * @see Author's
 * <a href="linkedin.com/in/oleh-kurachenko-6b025b111">LinkedIn</a>
 */

#include <iostream>
#include <cstdint>

using namespace std;

int main() {
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    size_t n;
    cin >> n;
    size_t result{0};
    for (size_t i{0}; i < n; ++i)
        for (size_t j{0}; j < n; ++j) {
            uint32_t temp;
            cin >> temp;
            if (i > j && temp == 1)
                ++result;
        }
    cout << result;

    return 0;
}