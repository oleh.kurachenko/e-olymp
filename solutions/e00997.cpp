/**
 * @file e00997.cpp
 * @copyright (C) 2018 by Oleh Kurachenko.
 * See LICENCE file at the root of repository
 * @author Oleh Kurachenko <oleh.kurachenko@gmail.com>
 * @date Created 2018-10-15
 *
 * @see Author's <a href="gitlab.com/oleh.kurachenko">GitLab</a>
 * @see Author's
 * <a href="linkedin.com/in/oleh-kurachenko-6b025b111">LinkedIn</a>
 */

#include <iostream>
#include <cstdint>

#include <vector>
#include <queue>

using namespace std;

int main() {
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    size_t n;
    cin >> n;

    size_t x1, y1, x2, y2;
    cin >> x1 >> y1 >> x2 >> y2;
    --x1;
    --x2;
    --y1;
    --y2;

    vector<uint32_t> tree(n*n, UINT32_MAX);
    queue<size_t> bfsQueue;
    tree[x1 * n + y1] = 0;
    bfsQueue.push(x1 * n + y1);

    size_t endNode = x2 * n + y2;
    while (tree[endNode] == UINT32_MAX) {
        size_t x{ bfsQueue.front() / n };
        size_t y{ bfsQueue.front() % n };

        size_t position;

        position = (x - 1) * n + y - 2;
        if (x >= 1 && y >= 2 && tree[position] == UINT32_MAX) {
            tree[position] = tree[bfsQueue.front()] + 1;
            bfsQueue.push(position);
        }
        position = (x - 2) * n + y - 1;
        if (x >= 2 && y >= 1 && tree[position] == UINT32_MAX) {
            tree[position] = tree[bfsQueue.front()] + 1;
            bfsQueue.push(position);
        }

        position = (x + 1) * n + y - 2;
        if (x + 1 < n && y >= 2 && tree[position] == UINT32_MAX) {
            tree[position] = tree[bfsQueue.front()] + 1;
            bfsQueue.push(position);
        }
        position = (x + 2) * n + y - 1;
        if (x + 2 < n && y >= 1 && tree[position] == UINT32_MAX) {
            tree[position] = tree[bfsQueue.front()] + 1;
            bfsQueue.push(position);
        }

        position = (x + 1) * n + y + 2;
        if (x + 1 < n && y + 2 < n && tree[position] == UINT32_MAX) {
            tree[position] = tree[bfsQueue.front()] + 1;
            bfsQueue.push(position);
        }
        position = (x + 2) * n + y + 1;
        if (x + 2 < n && y + 1 < n && tree[position] == UINT32_MAX) {
            tree[position] = tree[bfsQueue.front()] + 1;
            bfsQueue.push(position);
        }

        position = (x - 1) * n + y + 2;
        if (x >= 1 && y + 2 < n && tree[position] == UINT32_MAX) {
            tree[position] = tree[bfsQueue.front()] + 1;
            bfsQueue.push(position);
        }
        position = (x - 2) * n + y + 1;
        if (x >= 2 && y + 1 < n && tree[position] == UINT32_MAX) {
            tree[position] = tree[bfsQueue.front()] + 1;
            bfsQueue.push(position);
        }

        bfsQueue.pop();
    }

    cout << tree[endNode] << endl;

    return 0;
}
