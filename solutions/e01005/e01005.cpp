/**
 * @file e01005.cpp
 * @copyright (C) 2018 by Oleh Kurachenko.
 * See LICENCE file at the root of repository
 * @author Oleh Kurachenko <oleh.kurachenko@gmail.com>
 * @date Created 2018-11-15
 * @date Submitted 2018-11-15
 *
 * @see Author's <a href="gitlab.com/oleh.kurachenko">GitLab</a>
 * @see Author's
 * <a href="linkedin.com/in/oleh-kurachenko-6b025b111">LinkedIn</a>
 */

#include <cstdint>

#include <iostream>

using namespace std;

int main() {
    size_t n;
    cin >> n;

    for (size_t i{0}; i < n; ++i) {
        uint64_t n, m;
        cin >> n >> m;

        if (n <= m) {
            cout << '1';
            continue;
        }

        if ((n - m) % (m + 1) == 1)
            cout << '2';
        else
            cout << '1';
    }
    cout << endl;
}
