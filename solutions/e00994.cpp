/**
 * @file e00994.cpp
 * @copyright (C) 2018 by Oleh Kurachenko.
 * See LICENCE file at the root of repository
 * @author Oleh Kurachenko <oleh.kurachenko@gmail.com>
 * @date Created 2018-10-16
 *
 * @see Author's <a href="gitlab.com/oleh.kurachenko">GitLab</a>
 * @see Author's
 * <a href="linkedin.com/in/oleh-kurachenko-6b025b111">LinkedIn</a>
 */

#include <iostream>
#include <cstdint>

#include <vector>

using namespace std;

int main() {
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    size_t n;
    cin >> n;

    vector<pair<size_t, size_t>> bridges;
    for (size_t i{0}; i < n; ++i)
        for (size_t j{0}; j < n; ++j) {
            uint32_t v;
            cin >> v;
            if (i > j && v)
                bridges.push_back(make_pair(i, j));
        }

    vector<uint8_t> colors(n, 0);
    for (size_t i{0}; i < n; ++i)
        cin >> colors[i];

    size_t result{0};
    for (const pair<size_t, size_t> bridge : bridges)
        if (colors[bridge.first] != colors[bridge.second])
            ++result;

    cout << result << endl;

    return 0;
}