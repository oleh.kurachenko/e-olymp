/**
 * @file e00137.cpp
 * @copyright (C) 2018 by Oleh Kurachenko.
 * See LICENCE file at the root of repository
 * @author Oleh Kurachenko <oleh.kurachenko@gmail.com>
 * @date Created 2018-11-15
 * @date Submitted 2018-11-15
 *
 * @see Author's <a href="gitlab.com/oleh.kurachenko">GitLab</a>
 * @see Author's
 * <a href="linkedin.com/in/oleh-kurachenko-6b025b111">LinkedIn</a>
 */

#include <cstdint>

#include <iostream>

using namespace std;

template <typename INT_T>
INT_T gcd(const INT_T a, const INT_T b) {
    return (b == 0) ? a : gcd(b, a % b);
}

int main() {
    ios::sync_with_stdio(false);

    size_t n;
    cin >> n;

    uint32_t result;
    cin >> result;
    for (size_t i{1}; i < n; ++i) {
        uint32_t temp;
        cin >> temp;

        result = gcd(result, temp);
    }

    cout << result << endl;
}
 
