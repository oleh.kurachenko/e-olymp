/**
 * @file e00995.cpp
 * @copyright (C) 2018 by Oleh Kurachenko.
 * See LICENCE file at the root of repository
 * @author Oleh Kurachenko <oleh.kurachenko@gmail.com>
 * @date Created 2018-10-16
 *
 * @see Author's <a href="gitlab.com/oleh.kurachenko">GitLab</a>
 * @see Author's
 * <a href="linkedin.com/in/oleh-kurachenko-6b025b111">LinkedIn</a>
 */

#include <iostream>
#include <cstdint>

#include <vector>

using namespace std;

int main() {
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    size_t n;
    cin >> n;

    vector<vector<uint16_t>> map(n);
    for (size_t i{0}; i < n; ++i) {
        map[i].resize(n);
        for (size_t j{0}; j < n; ++j)
            cin >> map[i][j];
    }

    uint16_t minRoute{UINT16_MAX};
    size_t minx, miny, minz;

    for (size_t i{0}; i < n; ++i)
        for (size_t j{0}; j < n; ++j)
            for (size_t k{0}; k < n; ++k)
                if (i != k && i != j && j != k) {
                    uint16_t route(map[i][j] + map[j][k] + map[k][i]);
                    if (route < minRoute) {
                        minRoute = route;
                        minx = i + 1;
                        miny = j + 1;
                        minz = k + 1;
                    }
                }

    cout << minx << ' ' << miny << ' ' << minz << endl;

    return 0;
}
