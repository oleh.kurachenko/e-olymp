/**
 * @file e00005.cpp
 * @copyright (C) 2018 by Oleh Kurachenko.
 * See LICENCE file at the root of repository
 * @author Oleh Kurachenko <oleh.kurachenko@gmail.com>
 * @date Created 2018-11-15
 * @date Submitted 2018-11-15
 *
 * @see Author's <a href="gitlab.com/oleh.kurachenko">GitLab</a>
 * @see Author's
 * <a href="linkedin.com/in/oleh-kurachenko-6b025b111">LinkedIn</a>
 */

#include <cstdint>

#include <iostream>
#include <vector>

using namespace std;

int main() {

    vector<uint64_t> d(51);

    d[1] = 1;
    d[2] = 4;
    d[3] = 12;
    d[4] = 24;
    d[5] = 36;
    d[6] = 60;
    d[7] = 192;
    d[8] = 120;
    d[9] = 180;
    d[10] = 240;
    d[11] = 576;
    d[12] = 360;
    d[13] = 1296;
    d[14] = 900;
    d[15] = 720;
    d[16] = 840;
    d[17] = 9216;
    d[18] = 1260;
    d[19] = 786432;
    d[20] = 1680;
    d[21] = 2880;
    d[22] = 15360;
    d[23] = 3600;
    d[24] = 2520;
    d[25] = 6480;
    d[26] = 61440;
    d[27] = 6300;
    d[28] = 6720;
    d[29] = 2359296;
    d[30] = 5040;
    d[31] = 3221225472;
    d[32] = 7560;
    d[33] = 46080;
    d[34] = 983040;
    d[35] = 25920;
    d[36] = 10080;
    d[37] = 206158430208;
    d[38] = 32400;
    d[39] = 184320;
    d[40] = 15120;
    d[41] = 44100;
    d[42] = 20160;
    d[43] = 5308416;
    d[44] = 107520;
    d[45] = 25200;
    d[46] = 2985984;
    d[47] = 9663676416;
    d[48] = 27720;
    d[49] = 233280;
    d[50] = 45360;

    size_t k;
    cin >> k;
    cout << d[k] << endl;
}