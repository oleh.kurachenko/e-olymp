/**
 * @file e00005.cpp
 * @copyright (C) 2018 by Oleh Kurachenko.
 * See LICENCE file at the root of repository
 * @author Oleh Kurachenko <oleh.kurachenko@gmail.com>
 * @date Created 2018-11-15
 *
 * @see Author's <a href="gitlab.com/oleh.kurachenko">GitLab</a>
 * @see Author's
 * <a href="linkedin.com/in/oleh-kurachenko-6b025b111">LinkedIn</a>
 */

#include <cstdint>
#include <cmath>

#include <iostream>
#include <vector>

#include <set>

using namespace std;

int main() {
    ios::sync_with_stdio(false);

    vector<uint64_t> min_num(51, UINT64_MAX);
    size_t not_found{49};

    min_num[1] = 1;

    for (uint64_t i = 4; (i < SIZE_MAX) && not_found != 3; ++i) {
        double sqrt_i{ sqrt(i) };
        uint64_t lim_sqrt_i(static_cast<uint64_t>(sqrt_i));

        if (lim_sqrt_i * lim_sqrt_i == i)
            ++lim_sqrt_i;

        size_t result{0};
        for (uint64_t j = 1; j <= lim_sqrt_i && result <= 51; ++j)
            if (i % j == 0)
                ++result;

        if (result <= 50 && min_num[result] == UINT64_MAX) {
            min_num[result] = i;
            --not_found;
            cerr << "found " << i << " for position " << result << ", "
                 << not_found << " not found." << endl;
        }
    }

    for (uint64_t i = 1024 * 256; (i < SIZE_MAX) && not_found != 1;
            i += 1024 * 256) {
        double sqrt_i{ sqrt(i) };
        uint64_t lim_sqrt_i(static_cast<uint64_t>(sqrt_i));

        if (lim_sqrt_i * lim_sqrt_i == i)
            ++lim_sqrt_i;

        size_t result{0};
        for (uint64_t j = 1; j <= lim_sqrt_i && result <= 51; ++j)
            if (i % j == 0)
                ++result;

        if (result <= 50 && min_num[result] == UINT64_MAX) {
            min_num[result] = i;
            --not_found;
            cerr << "found " << i << " for position " << result << ", "
                 << not_found << " not found." << endl;
        }
    }

    for (uint64_t i = 1024 * 1024 * 64; (i < SIZE_MAX) && not_found;
            i += 1024 * 1024 * 64) {
        double sqrt_i{ sqrt(i) };
        uint64_t lim_sqrt_i(static_cast<uint64_t>(sqrt_i));

        if (lim_sqrt_i * lim_sqrt_i == i)
            ++lim_sqrt_i;

        size_t result{0};
        for (uint64_t j = 1; j <= lim_sqrt_i && result <= 51; ++j)
            if (i % j == 0)
                ++result;

        if (result <= 50 && min_num[result] == UINT64_MAX) {
            min_num[result] = i;
            --not_found;
            cerr << "found " << i << " for position " << result << ", "
                 << not_found << " not found." << endl;
        }
    }

    for (size_t i = 1; i <= 50; ++i)
        cout << "d[" << i << "] = " << min_num[i] << ";" << endl;

    return 0;
}
