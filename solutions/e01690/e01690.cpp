/**
 * @file e01690.cpp
 * @copyright (C) 2018 by Oleh Kurachenko.
 * See LICENCE file at the root of repository
 * @author Oleh Kurachenko <oleh.kurachenko@gmail.com>
 * @date Created 2018-11-15
 * @date Submitted 2018-11-15
 *
 * @see Author's <a href="gitlab.com/oleh.kurachenko">GitLab</a>
 * @see Author's
 * <a href="linkedin.com/in/oleh-kurachenko-6b025b111">LinkedIn</a>
 */

#include <cstdint>
#include <cmath>

#include <iostream>

using namespace std;

uint16_t gcd(const uint16_t a, const uint16_t b) {
    return (b == 0) ? a : gcd(b, a % b);
}

int main() {
    ios::sync_with_stdio(false);

    uint16_t v1, v2, v3, v4;
    cin >> v1 >> v2 >> v3 >> v4;

    cout << (100 / gcd(v1, gcd(v2, gcd(v3, v4))));

    return 0;
}
 
