/**
 * @file e01056.cpp
 * @copyright (C) 2018 by Oleh Kurachenko.
 * See LICENCE file at the root of repository
 * @author Oleh Kurachenko <oleh.kurachenko@gmail.com>
 * @date Created 2018-10-16
 *
 * @see Author's <a href="gitlab.com/oleh.kurachenko">GitLab</a>
 * @see Author's
 * <a href="linkedin.com/in/oleh-kurachenko-6b025b111">LinkedIn</a>
 */

#include <iostream>
#include <cstdint>

#include <algorithm>
#include <vector>
#include <queue>
#include <unordered_map>
#include <unordered_set>

using namespace std;

class UnionFind {
public:
    UnionFind(const size_t n):
            unionsNumber(n),
            dest(n),
            depth(n, 0) {

        for (size_t i{0}; i < n; ++i)
            dest[i] = i;
    }

    size_t makeUnion(const unordered_set<size_t> &elements) {
        unionsNumber -= elements.size() - 1;
        size_t minDepthElement{*elements.begin()};

        for (const size_t element : elements)
            if (depth[element] < depth[minDepthElement])
                minDepthElement = element;

        for (const size_t element : elements)
            if (element != minDepthElement) {
                dest[element] = minDepthElement;
                depth[element] = depth[minDepthElement] + 1;
            }

        return minDepthElement;
    }

    size_t find(const size_t element) {
        size_t elem{element};
        while (elem != dest[elem])
            elem = dest[elem];
        dest[element] = elem;
        depth[elem] = 0;
        return elem;
    }

    size_t unionsNumber;
    vector<size_t> dest;
    vector<size_t> depth;
};

int main() {
    ios::sync_with_stdio(false);
    cin.tie();
    cout.tie();

    size_t n;
    cin >> n;

    unordered_map<size_t, unordered_set<size_t>> graph(n);
    UnionFind unionFind(n);

    for (size_t i{0}; i < n; ++i)
        graph[i] = unordered_set<size_t>();

    for (size_t i{1}; i < n; ++i) {
        size_t v1, v2;
        cin >> v1 >> v2;
        --v1;
        --v2;

        graph[v1].insert(v2);
        graph[v2].insert(v1);
    }

    vector<size_t> topology(n, SIZE_MAX);
    vector<size_t> depth(n);
    topology[0] = 0;
    depth[0] = 0;
    queue<size_t> bfs;

    bfs.push(0);

    while (!bfs.empty()) {
        for (const size_t node : graph[bfs.front()]) {
            if (topology[node] == SIZE_MAX) {
                topology[node] = bfs.front();
                depth[node] = depth[bfs.front()] + 1;
                bfs.push(node);
            }
        }
        bfs.pop();
    }

    size_t m;
    cin >> m;

    for (size_t i{0}; i < m; ++i) {
        size_t v1, v2;
        cin >> v1 >> v2;
        --v1;
        --v2;

        v1 = unionFind.find(v1);
        v2 = unionFind.find(v2);

        if (v1 == v2)
            continue;

        unordered_set<size_t> pathSet;

        while (v1 != v2) {
            if (depth[v1] < depth[v2]) {
                pathSet.insert(v2);
                v2 = unionFind.find(topology[v2]);
            } else {
                pathSet.insert(v1);
                v1 = unionFind.find(topology[v1]);
            }
        }
        pathSet.insert(v1);

        size_t newId = unionFind.makeUnion(pathSet);
        topology[newId] = topology[v1];
        depth[newId] = depth[v1];
    }

    cout << unionFind.unionsNumber - 1 << endl;

    return 0;
}